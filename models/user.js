/**
 * Created by PHI on 2/28/2016.
 */
var Schema = require('mongoose').Schema;
var mongoose = require('mongoose');

var user = new Schema({
    firstName: String,
    lastName: String,
    email: String,
    password: String
});

var userModel = mongoose.model('User', user);
module.exports = userModel;