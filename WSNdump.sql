--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: Data; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "Data" (
    id bigint NOT NULL,
    time_receive time with time zone NOT NULL,
    time_insert time with time zone NOT NULL,
    "NH4" double precision,
    "NO2" double precision,
    "NO3" double precision,
    "PO4" double precision,
    "Mn2" double precision,
    "Fe" double precision,
    "Asen" double precision,
    "SensorNode_idSensorNode" integer NOT NULL
);


ALTER TABLE "Data" OWNER TO postgres;

--
-- Name: Quan; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "Quan" (
    "idQuan" integer NOT NULL,
    name text NOT NULL,
    "Tinh_idTinh" integer NOT NULL
);


ALTER TABLE "Quan" OWNER TO postgres;

--
-- Name: SensorInfo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "SensorInfo" (
    "idSensor_info" integer NOT NULL,
    sensor_name text NOT NULL,
    model text NOT NULL,
    "desc" text,
    tem_conditional text,
    humi_conditional text,
    power_supply text,
    range_measurement double precision,
    manual_link text,
    datasheet_link text,
    unit_measurement text,
    "SensorNode_idSensorNode" integer NOT NULL
);


ALTER TABLE "SensorInfo" OWNER TO postgres;

--
-- Name: SensorNode; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "SensorNode" (
    "idSensorNode" integer NOT NULL,
    "Address" text NOT NULL,
    latitude double precision NOT NULL,
    longtitude double precision NOT NULL,
    time_sample time with time zone NOT NULL,
    date_setup date NOT NULL
);


ALTER TABLE "SensorNode" OWNER TO postgres;

--
-- Name: SensorReport; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "SensorReport" (
    "idSensorNode_report" integer NOT NULL,
    from_time time with time zone,
    to_time time with time zone,
    "sensorName" text,
    status text,
    cause text,
    solution text,
    date_fix date,
    date_error date,
    other_cause text,
    "SensorNode_idSensorNode" integer NOT NULL
);


ALTER TABLE "SensorReport" OWNER TO postgres;

--
-- Name: Tinh; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "Tinh" (
    "idTinh" integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE "Tinh" OWNER TO postgres;

--
-- Name: XaPhuong; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "XaPhuong" (
    "idXaPhuong" integer NOT NULL,
    name text NOT NULL,
    "Quan_idQuan" integer NOT NULL
);


ALTER TABLE "XaPhuong" OWNER TO postgres;

--
-- Data for Name: Data; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "Data" (id, time_receive, time_insert, "NH4", "NO2", "NO3", "PO4", "Mn2", "Fe", "Asen", "SensorNode_idSensorNode") FROM stdin;
\.


--
-- Data for Name: Quan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "Quan" ("idQuan", name, "Tinh_idTinh") FROM stdin;
\.


--
-- Data for Name: SensorInfo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "SensorInfo" ("idSensor_info", sensor_name, model, "desc", tem_conditional, humi_conditional, power_supply, range_measurement, manual_link, datasheet_link, unit_measurement, "SensorNode_idSensorNode") FROM stdin;
\.


--
-- Data for Name: SensorNode; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "SensorNode" ("idSensorNode", "Address", latitude, longtitude, time_sample, date_setup) FROM stdin;
\.


--
-- Data for Name: SensorReport; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "SensorReport" ("idSensorNode_report", from_time, to_time, "sensorName", status, cause, solution, date_fix, date_error, other_cause, "SensorNode_idSensorNode") FROM stdin;
\.


--
-- Data for Name: Tinh; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "Tinh" ("idTinh", name) FROM stdin;
\.


--
-- Data for Name: XaPhuong; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "XaPhuong" ("idXaPhuong", name, "Quan_idQuan") FROM stdin;
\.


--
-- Name: Quan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "Quan"
    ADD CONSTRAINT "Quan_pkey" PRIMARY KEY ("idQuan");


--
-- Name: primary key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "XaPhuong"
    ADD CONSTRAINT "primary key" PRIMARY KEY ("idXaPhuong");


--
-- Name: primary key sensorNode; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "SensorNode"
    ADD CONSTRAINT "primary key sensorNode" PRIMARY KEY ("idSensorNode");


--
-- Name: primaryKey Data; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "Data"
    ADD CONSTRAINT "primaryKey Data" PRIMARY KEY (id);


--
-- Name: primaryKey sensorReport; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "SensorReport"
    ADD CONSTRAINT "primaryKey sensorReport" PRIMARY KEY ("idSensorNode_report");


--
-- Name: primarykey SensorInfo; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "SensorInfo"
    ADD CONSTRAINT "primarykey SensorInfo" PRIMARY KEY ("idSensor_info");


--
-- Name: tinh_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "Tinh"
    ADD CONSTRAINT tinh_name_key UNIQUE (name);


--
-- Name: tinh_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "Tinh"
    ADD CONSTRAINT tinh_pkey PRIMARY KEY ("idTinh");


--
-- Name: Data_SensorNode; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Data"
    ADD CONSTRAINT "Data_SensorNode" FOREIGN KEY ("SensorNode_idSensorNode") REFERENCES "SensorNode"("idSensorNode") ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: SensorInfo_SensorNode; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "SensorInfo"
    ADD CONSTRAINT "SensorInfo_SensorNode" FOREIGN KEY ("SensorNode_idSensorNode") REFERENCES "SensorNode"("idSensorNode") ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: SensorReport_SensorNode; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "SensorReport"
    ADD CONSTRAINT "SensorReport_SensorNode" FOREIGN KEY ("SensorNode_idSensorNode") REFERENCES "SensorNode"("idSensorNode") ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: Tinh_Quan; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Quan"
    ADD CONSTRAINT "Tinh_Quan" FOREIGN KEY ("Tinh_idTinh") REFERENCES "Tinh"("idTinh") ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: XaPhuong_Quan; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "XaPhuong"
    ADD CONSTRAINT "XaPhuong_Quan" FOREIGN KEY ("Quan_idQuan") REFERENCES "Quan"("idQuan") ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

