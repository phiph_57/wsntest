/**
 * Created by PHI on 2/28/2016.
 */

var io = require('socket.io')();

io.on('connection', function (client) {
    console.log("Client connected...");

    client.emit('message', {hello: 'world'});

    client.on('messages', function (data) {
        var nickname = client.nickname;

        client.broadcast.emit('messages', nickname + ": " + data);
        client.emit("messages", nickname + ": " + data);
    });

    client.on('join', function (name) {
        client.nickname = name;
    });
});

module.exports = io;