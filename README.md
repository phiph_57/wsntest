###1. Các module và ứng dụng cần cài đặt.
 - Nodejs
 - Mongodb
 - Expressjs ( framework của Nodejs)
 - Socket.io ( Thư viện socket cho ứng dụng realtime)


----------


###2. Các bước thực hiện
#### a. Cài đặt các phần mềm cần thiết(tự thực hiện):
- Cài đặt Nodejs
- Cài đặt Mongodb
- Cài đặt express framework ( Có thể sử dụng express-generator để tạo project mẫu hoặc dùng Webstorm IDE)
- Cài đặt thư viện socket.io và mongoose `npm install --save socket.io mongoose`
####b. Thực hiện cấu hình project.
- Kết nối đến mongodb qua module mongoose:
Tạo file `mongoose.js` theo đường dẫn `config\mongoose.js`. Nhớ tạo folder `config` nếu folder `config` chưa tồn tại. Nội dung code trong file như sau:
```
var mongoose = require('mongoose');/*call module*/
var dblink = 'mongodb://localhost/WSN';/*database connect path*/

mongoose.connect(dblink); /*connect*/

```
- Tạo config Socket.io:
Tạo file `socket.js` trong thư mục gốc. Với nội dung code như sau:
```

var io = require('socket.io')(); /*call module*/

/*listen event*/
io.on('connection', function (client) {
    console.log("Client connected...");
	
    client.emit('message', {hello: 'world'});/*emit event to client */

    client.on('messages', function (data) {/*listen event from client*/
        var nickname = client.nickname;

        client.broadcast.emit('messages', nickname + ": " + data);/*emit event to all client except me*/
        client.emit("messages", nickname + ": " + data);
    });

    client.on('join', function (name) {
        client.nickname = name;
    });
});

module.exports = io;
```
- Tạo model cho project:
Tạo folder `models` sau đó tạo 2 file mới là `user.js` và `index.js`. Nội dung từng file như sau:
```
/**
 * user.js Created by PHI on 2/28/2016.
 */
var Schema = require('mongoose').Schema;
var mongoose = require('mongoose');

var user = new Schema({
    firstName: String,
    lastName: String,
    email: String,
    password: String
});

var userModel = mongoose.model('User', user);
module.exports = userModel;
```
```
/**
 * index.js Created by PHI on 2/28/2016.
 */
var normalizedPath = require("path").join(__dirname, "./");
require("fs").readdirSync(__dirname+'/').forEach(function(file) {
    //require("./" + file);
    if (file.match(/\.js$/) !== null && file !== 'index.js') {
        console.log("./"+file);
        require("./"+file);
    }
});
```
- Kết nối config Socket.io:
Vào file `www` trong folder `bin`. Thêm 2 dòng code vào giữa file ( lưu ý đặt trước dòng code `module.exports`):
```
var io = require("../socket.js");
io.attach(server);
```
- Config mongoose kết nối đến database và tạo model:
Vào file `app.js` thêm hai dòng code bên dưới ( lưu ý đặt trước dòng code `module.exports`)
```
//connect db
require('./config/mongoose');
//require models
require('./models/');
```
- Viết tính năng realtime cho ứng dụng:
Thay thế toàn bộ code trong file `user.js` nằm trong thư mục `routes` bằng đoạn code bên dưới ( lưu ý đặt trước dòng code `module.exports`):
```
var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var user = mongoose.model('User');
var socket = require("../socket");

/* GET users listing. */
router.get('/', function (req, res, next) {
    //res.send('respond with a resource');
    user.find().exec(function (err, result) {
        console.log(result);
        if (err) return console.log(err);

        res.render("user", {data: result});
        socket.on('connection', function(client){
            client.emit('connected', 'Hello World');
        })
    });
});
/*Create new User and emit function to all client*/
router.post('/', function (req, res, next) {
    console.log(req.body);
    var newUser = new user();
    newUser.email = req.body.email;
    newUser.firstName = req.body.firstName;
    newUser.lastName = req.body.lastName;
    newUser.password = req.body.password;

    newUser.save(function (err) {
        if (err) console.log(err);
        socket.emit('update data', newUser);

        res.end("Save ok");
    })
});

module.exports = router;

```
- Tạo mới file `user.jade` nằm trong thư mục `views` và chèn đoạn code bên dưới:
```
//
   Created by PHI on 2/28/2016.

extends layout
block content
    table
        thead
            th email
            th first name
            th last name
            th password
        tbody#content
            each user in data
                tr
                    td=user.email
                    td=user.firstName
                    td=user.lastName
                    td=user.password
    script(src = "/socket.io/socket.io.js")
    script.
        var socket = io();

        socket.on('message',function(data){
            console.log(data);
        })
        socket.on('connected', function (data) {
            console.log(data);
        })
        socket.on('update data', function (data) {
            console.log(data);
            $("#content").append("<tr><td>" +
                    data.email +
                    "</td><td>" +
                    data.firstName +
                    "</td><td>" +
                    data.lastName +
                    "</td><td>" +
                    data.password +
                    "</td></tr>")
        });

```
####c. Cài đặt và chạy thử.
- Trở về thư mục gốc và chạy command `npm install`
- Sau khi lệnh cài đặt trên chạy xong thực hiện lệnh `npm start` để khởi chạy server.
- Truy cập vào địa chỉ `http:\\localhost:3000\users`. Ban đầu chưa có dữ liệu nên trang web trống trơn và chỉ có các header.
- Thực hiện thử nghiệm tính năng realtime:
Cài đặt postman để tạo request. Link cài đặt tại [đây](https://www.getpostman.com/).
Sau khi cài đặt mở postman và chọn các thông số như sau:
	- chuyển method từ `GET` sang `POST`
	- Chọn đường dẫn là `localhost:3000/users`
	- Tích vào ô `x-www-form-urlencoded`
	- Tạo 4 field theo thứ tự `email`, `firstName`, `lastName`, `password`với giá trị bất kì.
Ví dụ của request như hình dưới đây:
![Request Example](https://lh3.googleusercontent.com/-hzwe-yw_Iuo/VtJVqi9B8xI/AAAAAAAAAm8/vK6R7oGOtec/s0/Untitled.png "Untitled.png")
	- Sau khi request trả về `Save ok`. Trình duyệt sẽ tự động thêm nội dung ngay lập tức mà không hề tải lại trang. Hãy thử nhiều lần và mở 2 tab đồng thời để kiểm chứng.