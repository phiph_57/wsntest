/**
 * Created by PHI on 3/7/2016.
 */
'use strict';
let pg = require('pg');
let connectionURL = process.env.URL || 'postgres://postgres:Finsifyr0ck@localhost/WSN';

module.exports = {
    query: (query, cb) => {
        pg.connect(connectionURL, (err, client, done) => {
            client.query(query, (err, result) => {
                done();
                cb(err, result);
            });
        });
    },
    queryWithParam: (text, values, cb) => {
        pg.connect(connectionURL, (err, client, done) => {
            client.query(text, values, (err, result) => {
                done();
                cb(err, result);
            });
        });
    }
}